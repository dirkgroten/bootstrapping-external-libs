Run in Cygwin from the "src/libvpx/build" folder:

../configure --target=x86-win32-vs12 --enable-static-msvcrt --enable-vp8 --disable-vp8-encoder --disable-examples --disable-docs --enable-webm-io --enable-libyuv --enable-codec-srcs --disable-unit-tests

../configure --target=armv7-android-gcc --enable-static-msvcrt --enable-vp8 --disable-vp8-encoder --disable-examples --disable-docs --enable-webm-io --enable-libyuv --enable-codec-srcs --disable-unit-tests --sdk-path=/cygdrive/D/ndk

